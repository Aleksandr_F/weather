//
//  ViewController.m
//  testProgramm
//
//  Created by Alexandr on 20.05.16.
//  Copyright (c) 2016 Alexandr. All rights reserved.
//

#define DOCUMENTS [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]

#import "ViewController.h"
#import "KFOpenWeatherMapAPIClient.h"
#import "KFOWMWeatherResponseModel.h"
#import "KFOWMMainWeatherModel.h"
#import "KFOWMWeatherModel.h"
#import "KFOWMForecastResponseModel.h"
#import "KFOWMCityModel.h"
#import "KFOWMDailyForecastResponseModel.h"
#import "KFOWMDailyForecastListModel.h"
#import "KFOWMSearchResponseModel.h"
#import "KFOWMSystemModel.h"

@interface ViewController ()

@property (nonatomic, strong) KFOpenWeatherMapAPIClient *apiClient;

@end

@implementation ViewController

@synthesize jsonData;
@synthesize jsonDictionary;
@synthesize textLabel;
@synthesize tableLabel;
@synthesize weatherInformations;
bool increase = YES;

- (void)viewDidLoad{
    [super viewDidLoad];
    
    //////////////////////////////TextView
    self.textLabel = [[UITextView alloc] init];
    self.textLabel.frame = CGRectMake(0,
                                      0,
                                      self.view.frame.size.width,
                                      self.view.frame.size.height/4);
    self.textLabel.delegate = self;
    [self.view addSubview:textLabel];
    
    
    ////////////////////////////////TableView
    self.tableLabel = [[UITableView alloc] init];
    self.tableLabel.frame =CGRectMake(0,
                                      self.textLabel.frame.size.height,
                                      self.view.frame.size.width,
                                      460-self.textLabel.frame.size.height);
    self.tableLabel.delegate = self;
    self.tableLabel.dataSource = self;
    [self.view addSubview:tableLabel];
    /////////////////////////////////
        
    [self createFile];
    
    self.apiClient = [[KFOpenWeatherMapAPIClient alloc] initWithAPIKey:@"eba47effea88b18d5b67eae531209447" andAPIVersion:@"2.5"];
    
    [self openFile];
    
    [self Deserialized];    

}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:1.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    if (increase) {
        
        self.textLabel.frame = CGRectMake(0,
                                          0,
                                          textLabel.frame.size.width,
                                          textLabel.frame.size.height*2);
        
        self.tableLabel.frame = CGRectMake(0,
                                           tableLabel.frame.origin.y+textLabel.frame.size.height/2,
                                           tableLabel.frame.size.width,
                                           tableLabel.frame.size.height-textLabel.frame.size.height/2);
        
        
        increase = FALSE;
        
        
    }else{
        
        self.textLabel.frame = CGRectMake(0,
                                          0,
                                          textLabel.frame.size.width,
                                          textLabel.frame.size.height/2);
        
        self.tableLabel.frame = CGRectMake(0,
                                           textLabel.frame.size.height,
                                           tableLabel.frame.size.width,
                                           tableLabel.frame.size.height+textLabel.frame.size.height);
        
        increase = TRUE;
                
    }
    
    [UIView commitAnimations];
    return NO;
    
}

- (void) Deserialized{
    jsonDictionary = [NSJSONSerialization
                       JSONObjectWithData:jsonData
                       options:NSJSONReadingMutableContainers error:nil];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  10;
    
}

- (NSString *)tableView: (UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return @"City";
    
}

-(void) openFile{
    
    NSString *filePath = [DOCUMENTS stringByAppendingPathComponent:@"Dictionary.json"];
    
    jsonData = [NSData dataWithContentsOfFile:filePath];
    
}

-(void) createFile{
    

    NSFileManager *fileMng = [NSFileManager defaultManager];
    NSString *filePath = [DOCUMENTS stringByAppendingPathComponent:@"Dictionary.json"];
    
    if ([fileMng fileExistsAtPath:filePath] == NO)
    {
        
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    
        NSArray *arrayCities = [[NSArray alloc]
                                   initWithObjects:@"Moscow",@"Minsk",@"Prague",@"Berlin",@"Paris",@"Vienna",@"Amsterdam",@"London",@"Madrid",@"Kiev", nil];
    
        NSArray *arrayCode = [[NSArray alloc]
                                initWithObjects:@"Rus",@"Blr",@"Aut",@"Geu",@"Fra",@"Pol",@"Nld",@"Eng",@"Esp",@"Ukr", nil];
    
        NSArray *arrayInformation = [[NSArray alloc]
                                     initWithObjects:@"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", @"Some information about city", nil];
        NSArray *arrayTime = [[NSArray alloc] initWithObjects:@"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:00", @"Jun 01 2016 00:005", @"Jun 01 2016 00:00", nil];
    
        [dictionary setValue:arrayCode forKey:@"citesCode"];
        [dictionary setValue:arrayCities forKey:@"City"];
        [dictionary setValue:arrayInformation forKey:@"Information"];
        [dictionary setValue:arrayTime forKey:@"Time"];
    
    
            jsonData = [NSJSONSerialization
                                dataWithJSONObject:dictionary
                                options:NSJSONWritingPrettyPrinted
                               error:nil];
    
    
        NSString *filepath = [DOCUMENTS stringByAppendingPathComponent:@"Dictionary.json"];
        [jsonData writeToFile:filepath atomically:YES];
    
    }
    
    
}

-(void) saveFile{
    
    jsonData = [NSJSONSerialization
                      dataWithJSONObject:jsonDictionary
                      options:NSJSONWritingPrettyPrinted
                      error:nil];
    
    NSString *filepath = [DOCUMENTS stringByAppendingPathComponent:@"Dictionary.json"];
    [jsonData writeToFile:filepath atomically:YES];
    
}

-(NSString *) getCurrentTime{
    
    NSDate *time = [NSDate date];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MMM dd yyyy HH:mm"];
    NSString *currentTime = [dateFormatter stringFromDate:time];
    
    return currentTime;
}

-(NSDate *) changeTime:(NSString *) cityTime{
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"MMM dd yyyy HH:mm"];
    return [dateFormatter dateFromString:cityTime];
}

- (void) getWeather:(NSIndexPath *)indexPath{
    
    NSString *cityInfo = [[jsonDictionary objectForKey:@"Information"] objectAtIndex:indexPath.row];
    NSString *weatherInfo = [jsonDictionary objectForKey:[[jsonDictionary objectForKey:@"City"] objectAtIndex:indexPath.row]];
    self.textLabel.text = [NSString stringWithFormat:@"%@ \n\n %@", cityInfo, weatherInfo];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (fabs([[self changeTime:[[jsonDictionary objectForKey:@"Time"] objectAtIndex:indexPath.row]]  timeIntervalSinceDate:[self changeTime:[self getCurrentTime]]]) >60) {
        

    id city = [[jsonDictionary objectForKey:@"City"] objectAtIndex:indexPath.row];
                    
            [self.apiClient weatherForCityName:city
                               withResultBlock:^(BOOL success, id responseData, NSError *error)
             {
                 if (success)
                 {
                     KFOWMWeatherResponseModel *responseModel = (KFOWMWeatherResponseModel *)responseData;

                     
                     weatherInformations = [NSString stringWithFormat:@"Weather on %@ \n temperature: %@ K, \n humidity: %@%%rH, \n pressure: %@ mbar",
                                            [self getCurrentTime],
                                            responseModel.mainWeather.temperature,
                                            responseModel.mainWeather.humidity,
                                            responseModel.mainWeather.pressure ];
                     [jsonDictionary setValue:weatherInformations forKey:city];
                     [[jsonDictionary objectForKey:@"Time"] replaceObjectAtIndex:indexPath.row withObject:[self getCurrentTime]];

                     
                     [self saveFile];
                     
                     NSString *cityInfo = [[jsonDictionary objectForKey:@"Information"] objectAtIndex:indexPath.row];
                     NSString *weatherInfo = [jsonDictionary objectForKey:[[jsonDictionary objectForKey:@"City"] objectAtIndex:indexPath.row]];
                     self.textLabel.text = [NSString stringWithFormat:@"%@ \n\n %@", cityInfo, weatherInfo];
                     
                 }
                 else
                 {
                     NSLog(@" Error %@", city);
                 }
             }];
    }else{
        [self getWeather:indexPath];
    }



}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *result = nil;

    static NSString *CellIdentifier = @"CellIdentifier";
    
    result = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (result == nil) {
        
        result = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                        reuseIdentifier:CellIdentifier];
        
        result.detailTextLabel.textColor = [UIColor blackColor];
    }
    

    result.textLabel.text = [[jsonDictionary objectForKey:@"City"] objectAtIndex:indexPath.row];
    result.detailTextLabel.text = [[jsonDictionary objectForKey:@"cityCode"] objectAtIndex: indexPath.row];
    

    
    return result;
}


@end
