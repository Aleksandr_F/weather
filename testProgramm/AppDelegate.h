//
//  AppDelegate.h
//  testProgramm
//
//  Created by Alexandr on 20.05.16.
//  Copyright (c) 2016 Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
