//
//  ViewController.h
//  testProgramm
//
//  Created by Alexandr on 20.05.16.
//  Copyright (c) 2016 Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSData *jsonData;
    NSMutableDictionary *jsonDictionary;
    NSMutableString *weatherInformations;
    NSMutableArray *timeOfCities;
}



@property (strong, nonatomic) UITextView *textLabel;
@property (strong, nonatomic) UITableView *tableLabel;

@property (nonatomic, retain) NSData *jsonData;
@property (nonatomic, retain) NSMutableDictionary *jsonDictionary;
@property (nonatomic, retain) NSMutableString *weatherInformations;

@end
