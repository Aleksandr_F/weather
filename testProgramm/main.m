//
//  main.m
//  testProgramm
//
//  Created by Alexandr on 20.05.16.
//  Copyright (c) 2016 Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
